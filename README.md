# pl0

PL/0 front end, compiler and VM. 

Installation

    $ go get modernc.org/pl0/...

Documentation: [godoc.org/modernc.org/pl0](http://godoc.org/modernc.org/pl0)

PL/0 executor documentation: [godoc.org/modernc.org/pl0/pl0](http://godoc.org/modernc.org/pl0/pl0)
